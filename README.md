# Visão geral

O projeto é uma aplicação front-end com objetivo de demonstrar uma aplicação básica para pagamento on-line, utilizando a API [Stripe](https://stripe.com/en-br).

## Tecnologias

- [Ionic Framework](https://ionicframework.com/) é uma ferramenta que simplifica as equipes a criar e enviar aplicativos híbridos e progressivos da Web com várias plataformas, com facilidade.


# Setup da aplicação (local)

## Pré-requisito

Antes de rodar a aplicação é preciso garantir que as seguintes dependências estejam corretamente instaladas:
```
NodeJS v8.9.1
Ionic 3.19.0
Android 8
```

## Instalação da aplicação

Primeiramente, faça o clone do repositório:
```
git clone https://bitbucket.org/klebernox/app-payment-stripe/src/master/
```
Feito isso, acesse o projeto:
```
cd app-payment-stripe
```
É preciso compilar o código e baixar as dependências do projeto:
```
npm install
```
Finalizado esse passo, vamos iniciar a aplicação:
```
ionic serve
```
Pronto. A aplicação está disponível em http://localhost:8100
```
lint started ...
build dev finished in 10.80 s
watch ready in 10.94 s
dev server running: http://localhost:8100/
```


# APIs

O projeto disponibiliza a API de pagamento com o Stripe: Payment, onde utilizam o padrão Rest de comunicação, produzindo e consumindo arquivos no formato JSON.

## Preparando ambiente

É necessário a criação de uma conta no site do [Stripe](https://dashboard.stripe.com/login). 

Depois de fazer isso, você deve obter as chaves da API. Vá para a guia "Get your test API keys" no menu e você encontrará as chaves Pública e Secreta.

Abra o arquivo:
```
api.config.ts
```

Copie sua chave pública "Publishable key" na propriedade do json:
```
publicKey
```