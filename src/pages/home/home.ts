import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, Loading } from 'ionic-angular';
import { IonicPage } from 'ionic-angular/navigation/ionic-page';

import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { PaymentService } from '../../services/domain/payment.service';
import { PaymentDTO } from '../../models/payment.dto';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  formGroup: FormGroup;

  constructor(
    public navCtrl: NavController,
    private formBuilder: FormBuilder,
    private paymentService: PaymentService,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController) {

    this.formGroup = this.formBuilder.group({
      number: ['4242424242424242', [Validators.required]],
      expMonth: [12, [Validators.required]],
      expYear: [2020, [Validators.required]],
      cvc: ['220', [Validators.required]],
      amount: [, [Validators.required]]
    });
  }

  toPay() {
    let loader = this.showLoad();
    this.paymentService.toPay(this.formGroup.value)
      .then(token => {
        let payment: PaymentDTO = {
          token: token.id,
          amount: this.formGroup.value.amount
        }

        this.paymentService.toCharge(payment)
          .subscribe(response => {
            this.showMessage('Sucesso', 'Pagamento realizado com sucesso!', loader);
          }, error => {
            this.showMessage('Erro', 'Um erro ocorreu: ' + error.message, loader);
          });
      })
      .catch(error => {
        this.showMessage('Erro', 'Um erro ocorreu: ' + error.message, loader);
      });
  }

  showMessage(title: string, message: string, loader: Loading) {
    if (loader !== null) {
      loader.dismiss();
    }
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['Ok']
    });
    alert.present();
  }

  showLoad() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait..'
    });

    loading.present();
    return loading;
  }
}
