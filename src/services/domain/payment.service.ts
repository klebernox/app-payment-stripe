import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { PaymentDTO } from "../../models/payment.dto";
import { API_CONFIG } from "../../config/api.config";
import { CardDTO } from "../../models/card.dto";
import { Stripe, StripeCardTokenRes } from '@ionic-native/stripe';

@Injectable()
export class PaymentService {

    constructor(
        public http: HttpClient,
        private stripe: Stripe) {
    }

    toPay(card: CardDTO): Promise<StripeCardTokenRes> {
        this.stripe.setPublishableKey(API_CONFIG.publicKey);
        return this.stripe.createCardToken(card);
    }

    toCharge(payment: PaymentDTO) {
        return this.http.post(
            `${API_CONFIG.baseUrl}/payment/charge`,
            payment,
            {
                observe: 'response',
                responseType: 'text'
            }
        );
    }
}