export interface CardDTO {
    number: string;
    expMonth: number;
    expYear: number;
    cvc: string;
    amount: number;
}