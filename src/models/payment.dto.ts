export interface PaymentDTO {
    token: string;
    amount: number;
}